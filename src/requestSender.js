import axios from 'axios';
import config from '@/config.js';

class RequestSender {
    static GetBlogList() {
        return axios.get(`${config.HOST}/list`);
    }

    static Publish(data) {
        return axios.post(`${config.HOST}/publish`, data);
    }

    static Login(data) {
        return axios.post(`${config.HOST}/login`, data);
    }

    static Signup(data) {
    
        return axios.post(`${config.HOST}/signup`, data);
    }
}

export default RequestSender;
